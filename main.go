package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/map-service/proto/map"
)

func main() {
	srv := micro.NewService(

		micro.Name("map-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewMapServiceClient("map-service", client.DefaultClient)

	// Get all stores in ground floor
	{
		id := uniuri.New()

		start := time.Now()
		log.Println("Get Path From South Kiosk to Lacoste")
		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()

		resp, err := c.GetPath(ctx, &pb.Path{Start: &pb.Node{Id: "5c7498e31c9d44000049e4a0"}, Goal: &pb.Node{Id: "5c74970a1c9d44000049e497"}})

		if err != nil {
			log.Panicf("Error: Something went wrong when finding path - %v", err)
		}

		log.Printf("Nodes: %v", resp.Nodes)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
